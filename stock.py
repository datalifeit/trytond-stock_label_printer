# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import calendar
from datetime import datetime
from dateutil.relativedelta import relativedelta
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval, Not, Equal
from trytond.exceptions import UserError
from trytond.i18n import gettext

CONVERSION = {
    '0': 0,
    '1': 1,
    '2': 2,
    '3': 3,
    '4': 4,
    '5': 5,
    '6': 6,
    '7': 7,
    '8': 8,
    '9': 9,
    'A': 10,
    'B': 11,
    'C': 12,
    'D': 13,
    'E': 14,
    'F': 15,
    'G': 16,
    'H': 17,
    'I': 18,
    'J': 19,
    'K': 20,
    'L': 21,
    'M': 22,
    'N': 23,
    'P': 24,
    'Q': 25,
    'R': 26,
    'S': 27,
    'T': 28,
    'U': 29,
    'V': 30,
    'W': 31,
    'X': 32,
    'Y': 33,
    'Z': 34
}


class Location(metaclass=PoolMeta):
    __name__ = 'stock.location'

    printers = fields.One2Many('label.printer', 'location', 'Printers',
        states={'invisible': ~Eval('type').in_(['storage', 'production'])},
        depends=['type'])
    trace_code = fields.Char('Trace code', size=2,
        states={
            'readonly': Not(Equal(Eval('type'), 'production')),
            'invisible': Not(Equal(Eval('type'), 'production'))
        },
        depends=['type'])

    def get_packcode(self):
        return self._encode_trace()

    def _encode_trace(self):
        pool = Pool()
        Date = pool.get('ir.date')

        res = 'T' + self.trace_code[1:]
        _now = datetime.now()
        minutes = _now - datetime(year=Date.today().year, month=1, day=1)
        minutes = int(minutes.total_seconds() / 60)
        if calendar.isleap(datetime.today().year):
            minutes += 527041
        _encoded_value = self.base35encode(minutes)
        return _encoded_value[0:2] + res + _encoded_value[2:]

    @classmethod
    def _decode_trace(cls, code, _year):
        line_code = '0' + code[3:4]
        pack_code = code
        year = int(_year)

        res = cls.search([('trace_code', '=', line_code)], limit=1)
        if not res:
            return None, None

        minutes = cls.base35decode(pack_code[:2] + pack_code[4:])
        if minutes > 527041:
            minutes -= 527041
        dt = datetime(year=year, month=1, day=1) + relativedelta(
            minutes=minutes)
        if code[2:3].upper() == 'H':
            dt += relativedelta(minutes=-60)
        return list(map(int, res)), dt

    @classmethod
    def base35encode(cls, number, alphabet=''.join(list(CONVERSION.keys()))):
        """Converts an integer to a base36 string"""
        if not isinstance(number, int):
            raise TypeError('number must be an integer')

        base35 = ''
        sign = ''

        if number < 0:
            sign = '-'
            number = -number

        if 0 <= number < len(alphabet):
            return sign + alphabet[number]

        while number != 0:
            number, i = divmod(number, len(alphabet))
            base35 = alphabet[i] + base35

        return sign + base35

    @classmethod
    def base35decode(cls, number):
        if 'O' in number.upper():
            raise UserError(gettext(
                'stock_label_printer.msg_invalid_trace',
                code=number))
        value = 0
        new_number = number[::-1].upper()
        for x in range(0, len(new_number)):
            value += pow(35, x) * CONVERSION[new_number[x]]
        return value
