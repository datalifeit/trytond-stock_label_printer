# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import dateutil
from trytond.model import ModelView, fields
from trytond.pool import Pool, PoolMeta
from trytond.pyson import PYSONEncoder
from trytond.wizard import StateView, Button, StateReport
from trytond.transaction import Transaction
import datetime

__all__ = ['UnitLoadTrace', 'GetPackCode']


class GetPackCode(ModelView):
    """Unit load trace get pack code"""
    __name__ = 'stock.unit_load.trace.get_pack_code'

    code = fields.Char('Pack code', size=6, required=True)
    year = fields.Char('Year', size=4, required=True)

    @staticmethod
    def default_year(name=None):
        return str(datetime.datetime.now().year)


class UnitLoadTrace(metaclass=PoolMeta):
    __name__ = 'stock.unit_load.trace'

    get_pack_code = StateView('stock.unit_load.trace.get_pack_code',
        'stock_label_printer.unit_load_trace_get_pack_code_view_form',
        [Button('Cancel', 'end', 'tryton-cancel'),
         Button('Print', 'print_', 'tryton-print'),
         Button('Trace', 'backward_', 'tryton-find', default=True)],)
    print_ = StateReport('stock.unit_load.trace_report')

    def transition_start(self):
        active_model = Transaction().context['active_model']
        if active_model == 'ir.ui.menu':
            return 'get_pack_code'
        return super(UnitLoadTrace, self).transition_start()

    def decode_packcode(self):
        return self._decode_packcode(self.get_pack_code.code.upper(),
            self.get_pack_code.year)

    @classmethod
    def _decode_packcode(cls, packcode, year):
        pool = Pool()
        Location = pool.get('stock.location')
        return Location._decode_trace(packcode.upper(), year)

    def do_backward_(self, action):
        pool = Pool()
        Location = pool.get('stock.location')
        Company = pool.get('company.company')

        if Transaction().context['active_model'] != 'stock.unit_load':
            production_locations, end_date = self.decode_packcode()

            if not production_locations or not end_date:
                return action, {}

            company = Company(Transaction().context['company'])
            lzone = (dateutil.tz.gettz(company.timezone) if company.timezone
                else dateutil.tz.tzutc())
            szone = dateutil.tz.tzutc()
            _date = end_date.replace(tzinfo=szone).astimezone(lzone).replace(
                tzinfo=None)
            action['name'] += ' - %s (%s) %s @ %s' % (
                self.get_pack_code.code.upper(),
                self.get_pack_code.year,
                Location(production_locations[0]).rec_name
                    if production_locations else None,
                _date)

            traced_unit_loads_ids = self._get_ul_backward_trace(None,
                packcode=self.get_pack_code.code, year=self.get_pack_code.year)
            action['pyson_domain'] = PYSONEncoder().encode(
                [('id', 'in', list(set(traced_unit_loads_ids)) or [0])])
            return action, {}
        return super(UnitLoadTrace, self).do_backward_(action)

    @classmethod
    def _get_ul_backward_trace_domain(cls, unit_load, **kwargs):
        if not unit_load:
            assert kwargs.get('packcode') and kwargs.get('year')
            packcode = kwargs.get('packcode')
            year = kwargs.get('year')

            production_locations, end_date = cls._decode_packcode(
                packcode, year)
            if not production_locations or not end_date:
                return []
            start_date = end_date
            return [
                ('start_date', '<=', end_date),
                ('end_date', '>=', start_date),
                ('to_location', 'in', production_locations),
                ('state', '=', 'done')]

        return super(UnitLoadTrace, cls)._get_ul_backward_trace_domain(
            unit_load, **kwargs)

    def do_print_(self, action):
        traced_unit_loads_ids = set(self._get_ul_backward_trace(None,
            packcode=self.get_pack_code.code, year=self.get_pack_code.year))
        action['pyson_domain'] = PYSONEncoder().encode([
            ('id', 'in', list(traced_unit_loads_ids))])
        return action, {
            'ids': list(traced_unit_loads_ids),
            'packcode': self.get_pack_code.code,
            'packcode_year': self.get_pack_code.year
        }
